﻿DROP SCHEMA `rent`;
CREATE SCHEMA `rent` DEFAULT CHARACTER SET utf8 ;

CREATE  TABLE IF NOT EXISTS `rent`.`Client` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `account` VARCHAR(45) NULL ,
  `isActive` BOOLEAN NOT NULL , 
  PRIMARY KEY (`id`) );

CREATE  TABLE IF NOT EXISTS `rent`.`Good` (
  `inventID` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `type` VARCHAR(45) NULL ,
  `dateTaken` DATE NULL ,
  `dateReturned` DATE NULL ,
  `client_id` INT ,
  PRIMARY KEY (`inventID`) );

delete from `rent`.`Client`;
delete from `rent`.`Good`;
INSERT INTO `rent`.`Client` (`name`, `account`, `isActive`) VALUES ('Иванов Петюня', '520',false);
INSERT INTO `rent`.`Client` (`name`, `account`, `isActive`) VALUES ('Хмырь', '20', true);
INSERT INTO `rent`.`Client` (`name`, `account`, `isActive`) VALUES ('тов. майор', '5200', true);
INSERT INTO `rent`.`Client` (`name`, `account`, `isActive`) VALUES ('Хмырь младший', '20', true);
INSERT INTO `rent`.`Good` (`name`, `type`, `dateTaken`,`dateReturned`, `client_id`) VALUES ('Кепочка очень модная', 'хипстерский наборчик', '2017-03-02', null, 2);
INSERT INTO `rent`.`Good` (`name`, `type`, `dateTaken`,`dateReturned`, `client_id`) VALUES ('Грипсы', 'велоаксессуары', '2017-03-02', null, null);
INSERT INTO `rent`.`Good` (`name`, `type`, `dateTaken`,`dateReturned`, `client_id`) VALUES ('Насос 2л 43424', 'велоаксессуары', '2017-03-03', null, 2);
INSERT INTO `rent`.`Good` (`name`, `type`, `dateTaken`,`dateReturned`, `client_id`) VALUES ('Насос 3л 542344', 'велоаксессуары', '2017-03-03', null, 3);
INSERT INTO `rent`.`Good` (`name`, `type`, `dateTaken`,`dateReturned`, `client_id`) VALUES ('Зеркало', 'велоаксессуары', '2017-03-06', '2017-03-09', 4);
INSERT INTO `rent`.`Good` (`name`, `type`, `dateTaken`,`dateReturned`, `client_id`) VALUES ('Лонгборд', 'хипстерский наборчик', '2017-03-01', '2017-03-01', '1');

use rent; 
select * from Client right join Good  on Client.id=Good.client_id;