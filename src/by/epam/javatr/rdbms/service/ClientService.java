package by.epam.javatr.rdbms.service;

import java.util.Set;

import by.epam.javatr.rdbms.bean.Client;
import by.epam.javatr.rdbms.bean.Good;
import by.epam.javatr.rdbms.bean.GoodsSet;
import by.epam.javatr.rdbms.service.exception.ServiceException;

public interface ClientService {
	public boolean addClient (Client c)  throws ServiceException ;
	public Set<Client> getClientByName (String n) throws ServiceException ;
	public Client getClientById (int id) throws ServiceException ;
	public void delClient (Client c) throws ServiceException ;
	public Set<Client> getAllClients () throws ServiceException ;
	public boolean giveGoodToClient(Good g, Client c) throws ServiceException ;
	public boolean recievGoodFromClient(Good g, Client c) throws ServiceException ;
	public GoodsSet getGoods(Client c) throws ServiceException ;

}
