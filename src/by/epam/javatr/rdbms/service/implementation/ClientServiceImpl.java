package by.epam.javatr.rdbms.service.implementation;

import java.util.Set;

import by.epam.javatr.rdbms.bean.Client;
import by.epam.javatr.rdbms.bean.Good;
import by.epam.javatr.rdbms.bean.GoodsSet;
import by.epam.javatr.rdbms.dao.ClientDao;
import by.epam.javatr.rdbms.dao.exception.DaoException;
import by.epam.javatr.rdbms.dao.factory.DaoFactory;
import by.epam.javatr.rdbms.service.ClientService;
import by.epam.javatr.rdbms.service.exception.ServiceException;

/**
 * @author Sergii_Kotov
 *
 */
public class ClientServiceImpl implements ClientService{
	
	/**
	 * чтоб не повторять одну и ту же проверку клиента
	 * @param c
	 * @return
	 */
	private boolean checkClient (Client c) {
		return (c==null)||(c.getAccount()<0)||(c.getId()<0);
	}
	
	@Override
	public boolean addClient(Client c) throws ServiceException {
		// проверка параметров 
		if (checkClient (c)) {
			throw new ServiceException("Wrong client parameters!");
		}
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			ClientDao ClDao = daoObjectFactory.getClientDao();
			return ClDao.addClient(c);
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

	@Override
	public Set<Client> getClientByName(String n) throws ServiceException  {
		// проверка параметров для имени? что тут можно проверить?
		//просто вызываем запрос
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			ClientDao ClDao = daoObjectFactory.getClientDao();
			return ClDao.getClientByName(n);
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

	@Override
	public Client getClientById(int id) throws ServiceException  {
		// проверка параметров для id
		if (id<=0) {
			throw new ServiceException("Wrong client parameters!");
		}
		//просто вызываем запрос
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			ClientDao ClDao = daoObjectFactory.getClientDao();
			return ClDao.getClientById(id);
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

	@Override
	public void delClient(Client c)  throws ServiceException {
		// проверка параметров для id
		if (checkClient (c)) {
			throw new ServiceException("Wrong client parameters!");
		}
		//просто вызываем запрос
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			ClientDao ClDao = daoObjectFactory.getClientDao();
			ClDao.delClient(c);
		} catch (DaoException e){
			throw new ServiceException(e);
		}		
	}

	@Override
	public Set<Client> getAllClients()  throws ServiceException {
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			ClientDao ClDao = daoObjectFactory.getClientDao();
			return ClDao.getAllClients();
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean giveGoodToClient(Good g, Client c) throws ServiceException  {
		if (checkClient (c)||(g==null) ) {
			throw new ServiceException("Wrong client parameters!");
		}
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			ClientDao ClDao = daoObjectFactory.getClientDao();
			return ClDao.giveGoodToClient(g,c);
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean recievGoodFromClient(Good g, Client c) throws ServiceException  {
		if (checkClient (c)||(g==null) ) {
			throw new ServiceException("Wrong client parameters!");
		}
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			ClientDao ClDao = daoObjectFactory.getClientDao();
			return ClDao.recievGoodFromClient(g,c);
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

	@Override
	public GoodsSet getGoods(Client c)  throws ServiceException {
		if (checkClient (c)) {
			throw new ServiceException("Wrong client parameters!");
		}
		try {
			DaoFactory daoObjectFactory = DaoFactory.getInstance();
			ClientDao ClDao = daoObjectFactory.getClientDao();
			return ClDao.getGoods(c);
		} catch (DaoException e){
			throw new ServiceException(e);
		}
	}

}
