package by.epam.javatr.rdbms.service.factory;

import by.epam.javatr.rdbms.service.ClientService;
import by.epam.javatr.rdbms.service.GoodService;
import by.epam.javatr.rdbms.service.implementation.ClientServiceImpl;
import by.epam.javatr.rdbms.service.implementation.GoodServiceImpl;

public class ServiceFactory {
	private static final ServiceFactory instance = new ServiceFactory();
	private final ClientService clientService = new ClientServiceImpl();
	private final GoodService goodService = new GoodServiceImpl();
	private ServiceFactory(){}
	public static ServiceFactory getInstance(){
	return instance;
	}
	public ClientService getCLientService(){
	return clientService;
	}
	public GoodService getGoodService(){
	return goodService;
	}
}
