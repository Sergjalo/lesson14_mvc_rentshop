package by.epam.javatr.rdbms.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;


import by.epam.javatr.rdbms.bean.Client;
import by.epam.javatr.rdbms.bean.Good;
import by.epam.javatr.rdbms.bean.GoodsSet;
import by.epam.javatr.rdbms.dao.ClientDao;
import by.epam.javatr.rdbms.dao.exception.DaoException;

/***
 * пока реализован только поиск по имени
 * и вывод всех клиентов 
 * !!!!!!!!!!!!!!!!
 * @author Sergii_Kotov
 *
 */
public class SQLClient implements ClientDao{
	Connection con;
	/***
	 * в фабрике установлена связь с базой - берем ее оттуда
	 * @param c 
	 */
	public SQLClient (Connection c){
		con=c;
	}

	@Override
	public boolean addClient (Client c) throws DaoException {
		return true;
	}
	@Override
	public Set<Client> getClientByName (String n)  throws DaoException {
		HashSet<Client> hs= new HashSet<Client>();
        String sql = "select c.* , ( select count(inventID) from Good where c.id=Good.client_id) as cntGoods from Client as c where name like '%"+n+"%'";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
            	Client cl = new Client();
                cl.setId(rs.getInt("id"));
                cl.setName(rs.getString("name"));
                cl.setAccount(rs.getDouble("account"));
                cl.setActive(rs.getBoolean("isActive"));		
                cl.setGoodsQnt(rs.getInt("cntGoods"));
                hs.add(cl);
            }            
            
        } catch (Exception e) {
        }
		return hs;
	}
	
	@Override
	public Client getClientById (int id) throws DaoException {
		return new Client();
	}
	@Override
	public void delClient (Client c) throws DaoException {
		
	}
	@Override
	public Set<Client> getAllClients () throws DaoException {
		HashSet<Client> hs= new HashSet<Client>();
        String sql = "select c.* , ( select count(inventID) from Good where c.id=Good.client_id) as cntGoods from Client as c";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
            	Client cl = new Client();
                cl.setId(rs.getInt("id"));
                cl.setName(rs.getString("name"));
                cl.setAccount(rs.getDouble("account"));
                cl.setActive(rs.getBoolean("isActive"));		
                cl.setGoodsQnt(rs.getInt("cntGoods"));
                hs.add(cl);
            }            
            
        } catch (Exception e) {
        }
		return hs;
	}
	
	@Override
	public boolean giveGoodToClient(Good g, Client c) throws DaoException {
		return false;
	}
	@Override
	public boolean recievGoodFromClient(Good g, Client c) throws DaoException {
		return false;
	}
	@Override
	public GoodsSet getGoods(Client c) throws DaoException {
		return null;
	}
	
}
