package by.epam.javatr.rdbms.dao.implementation;

import java.sql.Connection;

import by.epam.javatr.rdbms.bean.Good;
import by.epam.javatr.rdbms.bean.GoodsSet;
import by.epam.javatr.rdbms.dao.GoodDao;
import by.epam.javatr.rdbms.dao.exception.DaoException;

/***
 * ЗАПОЛНИТЬ!!!!!!!!!!!!!!!!!!!!!!
 * @author Sergii_Kotov
 *
 */
public class SQLGood implements GoodDao{
	Connection con;
	/***
	 * в фабрике установлена связь с базой - берем ее оттуда
	 * @param c 
	 */
	public SQLGood (Connection c){
		con=c;
	}
	
	@Override
	public boolean addGood (Good g) throws DaoException {
		return false;
	}
	@Override
	public GoodsSet getGoodByName (String n) throws DaoException {
		return null;
	}
	@Override
	public Good getGoodById (int id) throws DaoException {
		return null;
	}
	@Override
	public void delGood (GoodsSet g) throws DaoException {
	}
	@Override
	public GoodsSet getAllGoods () throws DaoException {
		return null;
	}
}
