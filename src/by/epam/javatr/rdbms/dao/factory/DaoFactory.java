package by.epam.javatr.rdbms.dao.factory;

import java.sql.Connection;
import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.sql.SQLException;

import by.epam.javatr.rdbms.dao.ClientDao;
import by.epam.javatr.rdbms.dao.GoodDao;
import by.epam.javatr.rdbms.dao.implementation.SQLClient;
import by.epam.javatr.rdbms.dao.implementation.SQLGood;

/***
 * коннектимся к базе здесь, один раз вместе с созданием дёргалок для запросов
 * @author Sergii_Kotov
 *
 */
public final class DaoFactory {
    static private String user = "root";//Логин пользователя
    static private String password = "masterkey";//Пароль пользователя
    static private String url = "jdbc:mysql://localhost:3306/rent";//URL адрес
    static private String driver = "com.mysql.jdbc.Driver";//Имя драйвера

    private static final DaoFactory instance = new DaoFactory();
    // инициализируем в конструкторе, т.к. хочу передать соед. с базой 
    private ClientDao sqlClientImpl; 
    private GoodDao sqlGoodImpl; 
    private static Connection connection; 

    
    private DaoFactory(){
    	try {
			connection = DriverManager.getConnection(url, user, password);
		} catch (SQLException e1) {
			//e1.printStackTrace();
		}
    	try {
            Class.forName(driver);//Регистрируем драйвер
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    	sqlClientImpl = new SQLClient(connection);
    	sqlGoodImpl = new SQLGood(connection);
    }
    
    public static DaoFactory getInstance(){
    	return instance;
    }
    
    /**
     * получить конкретную реализацию в виде интерфейсной ссылки 
     * @return ссылка на объект, у которого есть методы для запроса товаров из хранилища
     */
    public GoodDao getGoodDao(){
    return sqlGoodImpl;
    }
    
    /**
     * получить конкретную реализацию в виде интерфейсной ссылки 
     * @return ссылка на объект, у которого есть методы для запроса клиентов из хранилища
     */
    public ClientDao getClientDao(){
    return sqlClientImpl;
    }
}
