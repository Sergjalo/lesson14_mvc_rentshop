package by.epam.javatr.rdbms.controller;

import by.epam.javatr.rdbms.controller.command.Command;

public class Controller {
	public String executeTask(String request){
		CommandProvider cp = CommandProvider.getInstance(); 
		Command cmd=cp.getCmd(request);
		if (cmd!=null) {
			return cmd.execute(request);
		} else {
			return "incorrect request";
		}
	}
}
