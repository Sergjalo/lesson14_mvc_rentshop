package by.epam.javatr.rdbms.controller.command.implementation;

import java.util.Set;

import by.epam.javatr.rdbms.bean.Client;
import by.epam.javatr.rdbms.controller.command.Command;
import by.epam.javatr.rdbms.service.ClientService;
import by.epam.javatr.rdbms.service.exception.ServiceException;
import by.epam.javatr.rdbms.service.factory.ServiceFactory;

public class GetClientByName implements Command{
	@Override
	public String execute(String request) {
		//"AddClient sName";
		String [] params=request.split(" ");
		String response = "";
		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		ClientService clientService = serviceFactory.getCLientService();
		try {
			Set<Client> sC= clientService.getClientByName(params[1]);
			for (Client c : sC) {
				response+=c+"\n";
			}
		} catch (ServiceException e) {
			// write log
			response = "Error getting client";
		}
		return response;
	}
}
