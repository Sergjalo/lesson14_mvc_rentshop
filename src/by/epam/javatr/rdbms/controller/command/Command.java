package by.epam.javatr.rdbms.controller.command;

public interface Command {
	public String execute(String request);
}
