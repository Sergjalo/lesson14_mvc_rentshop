package by.epam.javatr.rdbms;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import by.epam.javatr.rdbms.controller.Controller;

public class MySqlTest {
	/*
    static private String user = "root";//Логин пользователя
    static private String password = "masterkey";//Пароль пользователя
    static private String url = "jdbc:mysql://localhost:3306/samplebaseRent";//URL адрес daotalk
    static private String driver = "com.mysql.jdbc.Driver";//Имя драйвера
    */
    public static void main(String[] args) throws SQLException {
    	Controller ctrl =new Controller();
    	// all possible commands (tasks)
    	//	ADDCLIENT,GETCLIENTBYNAME, GETCLIENTBYID, DELCLIENT, GETALLCLIENTS, GIVEGOODTOCLIENT,RECIEVGOODFROMCLIENT,GETGOODS,ADDGOOD,GETGOODBYNAME,GETGOODBYID,DELGOOD,GETALLGOODS

    	/*
    	// ругнется - мало параметров
    	System.out.println(ctrl.executeTask("ADDCLIENT имя"));
    	// ругнется - не те параметры
    	System.out.println(ctrl.executeTask("ADDCLIENT имя w"));
    	// покажет ок если в базе не было
    	System.out.println(ctrl.executeTask("ADDCLIENT имя 55,6"));
		*/
    	// вернет найденных не смотря на лишний параметр в конце
    	// поиск возвращает набор а не одно значение
    	System.out.println("Поиск хмырей");
    	//System.out.println(ctrl.executeTask("GETCLIENTBYNAME Хмырь 55.6"));

    	System.out.println("Поиск всех клиентов");
    	System.out.println(ctrl.executeTask("GETALLCLIENTS"));
    	
    	/*
    	Connection connection = null;
    	connection = DriverManager.getConnection(url, user, password);
    	try {
            Class.forName(driver);//Регистрируем драйвер
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    	//select * from client where name='Хмырь'
    	List<Good> list =new LinkedList<Good>();
        String sql = "SELECT id, number, department FROM daotalk.Group";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
        	Good good = new Good();
            ResultSet rs = statement.executeQuery();
            //list = parseResultSet(rs);
            while (rs.next()) {
                good.setInventID(rs.getInt("id"));
                good.setName(rs.getString("department"));
                list.add(good);
            }            
            
        } catch (Exception e) {
        }
        for (Good f:list) {
        	System.out.println(f);
        }
        */
    }
}
