package by.epam.javatr.rdbms.bean;

import java.io.Serializable;
import java.util.Calendar;

public class Good implements Serializable{

	private static final long serialVersionUID = 1L;
	private int inventID;
	private String type;
	private String name;
	private Calendar dateTaken;
	private Calendar dateReturned;
	private Client whoKeep;
	
	public Good() {
		this.inventID = 0;
		this.type = "w";
		this.name = "f";
		this.dateTaken = Calendar.getInstance();
		this.dateReturned = Calendar.getInstance();
		this.whoKeep = null;
	}
	
	public Good(int inventID, String type, String name, Calendar dateTaken, Calendar dateReturned, Client whoKeep) {
		this.inventID = inventID;
		this.type = type;
		this.name = name;
		this.dateTaken = dateTaken;
		this.dateReturned = dateReturned;
		this.whoKeep = whoKeep;
	}
	
	@Override
	public String toString() {
		return "Good [inventID=" + inventID + ", type=" + type + ", name=" + name + ", dateTaken=" + dateTaken.getTime()
				+ ", dateReturned=" + dateReturned.getTime() + ", whoKeep=" + whoKeep + "]";
	}

	public int getInventID() {
		return inventID;
	}
	public void setInventID(int inventID) {
		this.inventID = inventID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Calendar getDateTaken() {
		return dateTaken;
	}
	public void setDateTaken(Calendar dateTaken) {
		this.dateTaken = dateTaken;
	}
	public Calendar getDateReturned() {
		return dateReturned;
	}
	public void setDateReturned(Calendar dateReturned) {
		this.dateReturned = dateReturned;
	}
	public Client getWhoKeep() {
		return whoKeep;
	}
	public void setWhoKeep(Client whoKeep) {
		this.whoKeep = whoKeep;
	}
	
	

}
